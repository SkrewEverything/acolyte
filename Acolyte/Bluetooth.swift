//
//  Bluetooth.swift
//  Brain
//
//  Created by Sri Harish on 14/04/17.
//  Copyright © 2017 Skrew Everything. All rights reserved.
//

import Foundation

class Bluetooth
{
    func available() -> Bool
    {
        return IOBluetoothPreferencesAvailable() == 1 ? true : false
    }
    
    func getStatus() -> [String:Bool]
    {
        let power = IOBluetoothPreferenceGetControllerPowerState()
        let discoverable = IOBluetoothPreferenceGetDiscoverableState()
        let result = ["power": power == 1 ? true: false,"discoverable": discoverable == 1 ? true: false]
        return result
    }
    
    func setStatus(options:[String:Bool])
    {
        for i in options
        {
            switch i.key
            {
                case "power":
                        power(status: i.value)
                case "discoverable":
                        discoverable(status: i.value)
                default:
                    print("Wrong Option: \(i.key)")
            }
        }
    }
    
    func openBTPref()
    {
        let p = Process()
        p.launchPath = "/bin/bash"
        p.arguments = ["-c","open /System/Library/PreferencePanes/Bluetooth.prefPane"]
        p.launch()
    }
    
    func power(status: Bool)
    {
        IOBluetoothPreferenceSetControllerPowerState(status ? 1 : 0)
    }
    
    func discoverable(status: Bool)
    {
        IOBluetoothPreferenceSetDiscoverableState(status ? 1 : 0)
    }
}
