//
//  WifiTable.swift
//  Acolyte
//
//  Created by Sri Harish on 16/04/17.
//  Copyright © 2017 Skrew Everything. All rights reserved.
//

import Cocoa

class WifiTable: NSViewController, NSTableViewDataSource, NSTableViewDelegate
{
    
    @IBOutlet var tableView: NSTableView!
    var wifiNames: NSMutableArray! = NSMutableArray()
    var securityTypes: NSMutableArray! = NSMutableArray()
    var signalStrength: NSMutableArray! = NSMutableArray()
    
    
    override func viewDidLoad()
    {
        
        let b = BrainStruct.brain.wifiList
        print("List:  ", b)
        var j = 0
        for i in b
        {
            if j == 0 || j == b.count-1
            {
                j += 1
                continue
            }
            j += 1
            var r = Regex(regex: "(.+)\\s..:", string: i.trimmingCharacters(in: .whitespacesAndNewlines))
            if let r1 = r.getCapturedGroups()?[0]
            {
                self.wifiNames.add(r1)
                print(r1)
                /*if r.getCapturedGroups()?.count == 8
                {
                    securityTypes.add(r.getCapturedGroups()![6] + " " + r.getCapturedGroups()![7])
                }
                else
                {
                    securityTypes.add(r.getCapturedGroups()![6])
                }*/
            }
            r = Regex(regex: ".*\\s(.*)", string: i.trimmingCharacters(in: .whitespacesAndNewlines))
            if r.getCapturedGroups()![0].lowercased() == "none"
            {
                securityTypes.add("Open")
            }
            else
            {
                securityTypes.add("Closed")
            }
            r = Regex(regex: "(-\\d+)", string: i.trimmingCharacters(in: .whitespacesAndNewlines) )
            if let signal = Int((r.getCapturedGroups()?[0])!)// nil found error happened once fix it
            {
                let s = -signal
                if s <= 30
                {
                    self.signalStrength.add("Amazing")
                }
                else if s > 30 && s <= 67
                {
                    self.signalStrength.add("Very Good")
                }
                else if s > 67 && s <= 70
                {
                    self.signalStrength.add("Okay")
                }
                else if s > 70 && s <= 80
                {
                    self.signalStrength.add("Not Good")
                }
                else
                {
                    self.signalStrength.add("Not Usable")
                }
                }
        }
    }
    
    
    func numberOfRows(in tableView: NSTableView) -> Int
    {
        return wifiNames.count
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView?
    {
        var text = ""
        var cellIdentifier = ""
        if tableColumn == tableView.tableColumns[0]
        {
            text = self.wifiNames.object(at: row) as! String
            cellIdentifier = "wifi-name"
        }
        else if tableColumn == tableView.tableColumns[1]
        {
            text = self.securityTypes.object(at: row) as! String
            cellIdentifier = "security"
        }
        else if tableColumn == tableView.tableColumns[2]
        {
            text = self.signalStrength.object(at: row) as! String
            cellIdentifier = "signal-strength"
        }
        if let cell = tableView.make(withIdentifier: cellIdentifier, owner: self) as? NSTableCellView
        {
            cell.textField?.stringValue = text
            return cell
        }
        return nil
    }
    
    func tableViewSelectionDidChange(_ notification: Notification)
    {
        let queue = DispatchQueue(label: "com.skreweverything.wifitable", qos: .userInteractive, attributes: .concurrent)
        

        if self.tableView.selectedRow >= 0
        {
            queue.async
            {
            let w = Wifi()
            let ssid = self.wifiNames.object(at: self.tableView.selectedRow) as! String
            CurrentWifi.ssid = ssid
            if (self.securityTypes.object(at: self.tableView.selectedRow) as! String) == "Open"
            {
                let _ = w.wifiConnect(ssid: ssid, pass: nil)
            }
            else
            {
                let pass = w.getPassword(ssid: ssid)
                if let p1 = pass
                {
                    let _ = w.wifiConnect(ssid: ssid, pass: p1)
                }
                else
                {
                    let storyBoard = NSStoryboard(name: "Main", bundle: nil) as NSStoryboard
                    let myViewController = storyBoard.instantiateController(withIdentifier: "wifi-pass") as! NSViewController
                    self.presentViewControllerAsSheet(myViewController)
                }
            }
                DispatchQueue.main.async
                {
                    self.tableView.deselectRow(self.tableView.selectedRow)
                }
            }
        }
    }
    
    
}
