//
//  StartStopViewController.swift
//  ProjectPrototype1
//
//  Created by Sri Harish on 16/03/17.
//  Copyright © 2017 Skrew Everything. All rights reserved.
//

import Cocoa

class StartStopViewController: NSViewController
{

    @IBOutlet var keyloggerImage: NSButton!
    @IBOutlet var webHistoryImage: NSButton!
    @IBOutlet var keyloggerStatus: NSTextField!
    @IBOutlet var webHistoryStatus: NSTextField!

    @IBOutlet var keyloggerLoading: NSProgressIndicator!
    
    @IBOutlet var webHistoryLoading: NSProgressIndicator!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do view setup here.
        view.wantsLayer = true
        view.layer?.cornerRadius = 10.0
        if Status.isKeyloggerRunning
        {
            keyloggerImage.image = #imageLiteral(resourceName: "pause")
            keyloggerStatus.stringValue = "\"Running\""
            
        }
        else
        {
            keyloggerImage.image = #imageLiteral(resourceName: "play")
            keyloggerStatus.stringValue = "\"Stopped\""
        }
        if Status.isWebHistoryRunning
        {
            webHistoryImage.image = #imageLiteral(resourceName: "pause")
            webHistoryStatus.stringValue = "\"Running\""
        }
        else
        {
            webHistoryImage.image = #imageLiteral(resourceName: "play")
            webHistoryStatus.stringValue = "\"Stopped\""
        }
        webHistoryLoading.usesThreadedAnimation = true
        keyloggerLoading.usesThreadedAnimation = true
    }
    
    @IBAction func keyloggerStartStop(_ sender: Any)
    {
        var flag = true
        let button = sender as! NSButton
        if Status.isKeyloggerRunning
        {
            keyloggerImage.image = #imageLiteral(resourceName: "play")
            flag = true
            keyloggerStatus.stringValue = "\"Stopping\""
            Status.isKeyloggerRunning = false
            Services.keylogger.terminate()
        }
        else
        {
            keyloggerImage.image = #imageLiteral(resourceName: "pause")
            flag = false
            keyloggerStatus.stringValue = "\"Starting\""
            Status.isKeyloggerRunning = true
            Services.keylogger.launch()
        }
        keyloggerLoading.startAnimation(nil)
        button.isEnabled = false
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false){ timer in
            button.isEnabled = true
            self.keyloggerLoading.stopAnimation(nil)
            if flag
            {
                self.keyloggerStatus.stringValue = "\"Stopped\""
            }
            else
            {
                self.keyloggerStatus.stringValue = "\"Running\""
            }
        }
        
    }

    @IBAction func webHistoryStartStop(_ sender: Any)
    {
        var flag = true
        let button = sender as! NSButton
        if Status.isWebHistoryRunning
        {
            webHistoryImage.image = #imageLiteral(resourceName: "play")
            flag = true
            webHistoryStatus.stringValue = "\"Stopping\""
            Status.isWebHistoryRunning = false
            Services.webHistory.stop()
        }
        else
        {
            webHistoryImage.image = #imageLiteral(resourceName: "pause")
            flag = false
            webHistoryStatus.stringValue = "\"Starting\""
            Status.isWebHistoryRunning = true
            Services.webHistory.startTimer(time: 5)
        }
        webHistoryLoading.startAnimation(nil)
        button.isEnabled = false
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false){ timer in
            button.isEnabled = true
            self.webHistoryLoading.stopAnimation(nil)
            if flag
            {
                self.webHistoryStatus.stringValue = "\"Stopped\""
            }
            else
            {
                self.webHistoryStatus.stringValue = "\"Running\""
            }
        }
    }
    
    @IBAction func done(_ sender: Any)
    {
        Windows.startStopWindow.close()
    }
    
}
