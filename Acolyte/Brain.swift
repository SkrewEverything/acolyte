//
//  Initial.swift
//  Brain
//
//  Created by Sri Harish on 08/04/17.
//  Copyright © 2017 Skrew Everything. All rights reserved.
//

import Foundation

class Brain
{
    var input: String
    var wifiList: [String] = []
    var wifivc: Bool = false
    init(input: String)
    {
        self.input = input
        let nb = SharedNB.getNBs["NB0"]
        let options = nb?.test(document: input)
        print("Classes: ",options!)
        let option = getClass(result: options!).first!.key
        print("Prediction: ",option)
        
        switch(option)
        {
        case "capabilities":
            capabilities()
        case "commands":
            commands()
        default:
            print("I dont know")
        }
    }
    func capabilities()
    {
        print("===In Capability===")
        func systemCommands()
        {
            
        }
        func realLife()
        {
            
        }
    }
    func commands()
    {
        print("===In Commands===")
        let nb = SharedNB.getNBs["NB2"]
        let options = nb?.test(document: input)
        print("Classes: ",options!)
        var option = getClass(result: options!).first!.key
        print("Prediction: ",option)
        if input.lowercased().contains(".com") // Just a work-around
        {
            option = "webpage"
        }
        switch(option)
        {
            case "volume":
                volume()
            case "wifi":
                wifi()
            case "bluetooth":
                bluetooth()
            case "brightness":
                brightness()
            case "logs":
                logs()
            case "apps":
                apps()
            case "webpage":
                webpage()
            default:
                print("I dont know")
        }

    }
/***** System Commands *******/
    func volume()
    {
        print("=====In Volume=====")
        let nb = SharedNB.getNBs["NB3"]
        let options = nb?.test(document: input)
        print("Classes: ",options!)
        let option = getClass(result: options!).first!.key
        print("Prediction: ",option)
        let v = Volume()
        switch(option)
        {
            case "mute-volume":
                print("mute volume")
                v.mute()
            case "increase-volume":
                v.increase()
                print("increase volume")
            case "decrease-volume":
                v.decrease()
                print("decrease volume  ")
            case "unmute-volume":
                print("unmute volume") // Write a document and train
                v.unmute()
            default:
                print("I don't know")
        }
    }
    func wifi()
    {
        let nb = SharedNB.getNBs["NB4"]
        let options = nb?.test(document: input)
        print("Classes: ",options!)
        let option = getClass(result: options!).first!.key
        print("Prediction: ",option)
        switch(option)
        {
            case "wifi-off":
                print("wifi off")
                let w = Wifi()
                w.wifiOff()
            case "wifi-on":
                print("wifi on")
                let w = Wifi()
                w.wifiOn()
            case "wifi-list":
                print("wifi list")
                let w = Wifi()
                self.wifiList = w.wifiList()
                self.wifivc = true
                //print("List: ", self.wifiList)
            case "wifi-connect":
                print("wifi connection")
            case "wifi-disconnect":
                print("wifi disconnect")
            default:
                print("I don't know")
        }
    }
    func bluetooth()
    {
        let nb = SharedNB.getNBs["NB5"]
        let options = nb?.test(document: input)
        print("Classes: ",options!)
        let option = getClass(result: options!).first!.key
        print("Prediction: ",option)
        switch(option)
        {
            case "bluetooth-off":
                print("bluetooth off")
                let b = Bluetooth()
                b.setStatus(options: ["power": false])
            case "bluetooth-on":
                print("bluetooth on")
                let b = Bluetooth()
                b.setStatus(options: ["power": true, "discoverable": true])
            case "bluetooth-list":
                print("bluetooth list")
                let b = Bluetooth()
                b.openBTPref()
            default:
                print("I don't know")
        }
    }
    func brightness()
    {
        let nb = SharedNB.getNBs["NB6"]
        let options = nb?.test(document: input)
        print("Classes: ",options!)
        let option = getClass(result: options!).first!.key
        print("Prediction: ",option)
        switch(option)
        {
            case "increase-brightness":
                print("brightness increase")
                let b = Brightness()
                b.setBrightness(level: b.getBrightness() + 0.3)
            case "decrease-brightness":
                print("brightness decrease")
                let b = Brightness()
                b.setBrightness(level: b.getBrightness() - 0.3)
            default:
                print("I don't know")
        }
    }
    func logs()
    {
        let nb = SharedNB.getNBs["NB7"]
        let options = nb?.test(document: input)
        print("Classes: ",options!)
        let option = getClass(result: options!).first!.key
        print("Prediction: ",option)
        switch(option)
        {
            case "logs":
                print("Show logs window")
                let sb = NSStoryboard(name: "Logs", bundle: nil)
                
                Windows.logWindow = sb.instantiateInitialController() as! NSWindowController?
                if Windows.logWindow?.window?.isVisible == false
                {
                    Windows.logWindow?.window?.setIsVisible(true)
            }

            default:
                print("I don't know")
        }
    }
    func apps()
    {
        let nb = SharedNB.getNBs["NB8"]
        let options = nb?.test(document: input)
        print("Classes: ",options!)
        let option = getClass(result: options!).first!.key
        print("Prediction: ",option)
        let a = Apps()
        switch(option)
        {
        case "open-app":
            print("open app")
            a.open(input: self.input) // returns true if found
        case "close-app":
            print("close app")
            a.close(input: self.input)
        case "close-all-apps":
            print("close all apps")
            a.closeAll()
        default:
            print("I don't know")
        }
    }
    func webpage()
    {
        let nb = SharedNB.getNBs["NB9"]
        let options = nb?.test(document: input)
        print("Classes: ",options!)
        let option = getClass(result: options!).first!.key
        print("Prediction: ",option)
        func getwebName(input: String) -> [String] // Need to improve this method. It gives output as words which are not found in NB training files. And we assume the words as correct app name due to no time
        {
            let listtoremove = ["a","an","the","of","named","name","is"]
            
            var r = input.components(separatedBy: .whitespacesAndNewlines)
            let c = try! String(contentsOfFile: Bundle.main.resourcePath! + "/Train/webpage.txt")
            let q = c.components(separatedBy: .whitespacesAndNewlines)
            let w = NSCountedSet(array: q)
            var unique = [String]()
            for i in listtoremove
            {
                var c = 0
                for j in r
                {
                    if i.lowercased() == j.lowercased()
                    {
                        r.remove(at: c)
                    }
                    c += 1
                }
            }
            //print(r)
            for i in r
            {
                if w.count(for: i) == 0
                {
                    unique.append(i)
                }
            }
            return unique
        }
        
        switch(option)
        {
        case "webpage-open":
            print("Open new tab")
            var u = getwebName(input: self.input)[0]
            if u.hasPrefix("http://") || u.hasPrefix("https://")
            {
                
            }
            else
            {
                u = "http://" + u
            }
            if u.hasSuffix(".com") || u.hasSuffix(".co.in") || u.hasSuffix(".in")
            {
                
            }
            else
            {
                u = u + ".com"
            }

            let p = Process()
            p.launchPath = "/bin/bash"
            p.arguments = ["-c", "osascript -e 'tell application \"Google Chrome\" to open location \"\(u)\"'"]
            p.launch()
        case "webpage-open-window":
            print("Open new window")
            var u = getwebName(input: self.input)[0]
            if u.hasPrefix("http://") || u.hasPrefix("https://")
            {
                
            }
            else
            {
                u = "http://" + u
            }
            if u.hasSuffix(".com") || u.hasSuffix(".co.in") || u.hasSuffix(".in")
            {
                
            }
            else
            {
                u = u + ".com"
            }

            let p = Process()
            p.launchPath = "/bin/bash"
            p.arguments = ["-c","osascript \"\(Bundle.main.resourcePath! + "/WebScripts/chrome_new_window.scpt")\" \(u)"]
            p.launch()
        case "webpage-reload":
            print("Reload webpage")
            let p = Process()
            p.launchPath = "/bin/bash"
            p.arguments = ["-c","osascript \"\(Bundle.main.resourcePath! + "/WebScripts/chrome_reload_curr_tab.scpt")\""]
            p.launch()
        case "webpage-bookmark":
            print("Bookmark tab")
            let p = Process()
            p.launchPath = "/bin/bash"
            p.arguments = ["-c","osascript \"\(Bundle.main.resourcePath! + "/WebScripts/chrome_bookmark_curr.scpt")\""]
            p.launch()
        case "webpage-back":
            print("Webpage Back")
            let p = Process()
            p.launchPath = "/bin/bash"
            p.arguments = ["-c","osascript \"\(Bundle.main.resourcePath! + "/WebScripts/chrome_goback_curr_tab.scpt")\""]
            p.launch()
        default:
            print("I don't know webpages")
        }
    }
/*****************************/
    
    func getClass(result: [String:Double]) -> [String:Double]
    {
        let final = result.flatMap({$0}).sorted { $0.0.value > $0.1.value }
        return [final[0].key:final[0].value]
    }
}
