//
//  Apps.swift
//  Brain
//
//  Created by Sri Harish on 16/04/17.
//  Copyright © 2017 Skrew Everything. All rights reserved.
//

import Foundation
import Cocoa
class Apps
{
    
    func find() -> [String]
    {
        var listOfApps = [""]
        let f = FileSystem(path: ["~","/Applications"], find: [".app"])
        for i in f.c
        {
            if i.hasSuffix(".app")
            {
                //print(i)
                let r = Regex(regex: "/.*/(.*\\.app)", string: i)
                if let r1 = r.getCapturedGroups()?[0]
                {
                    listOfApps.append(r1)
                }
            }
        }
        var liststr = ""
        for i in listOfApps
        {
            liststr = liststr + i + "\n"
        }
        liststr = liststr.trimmingCharacters(in: .whitespacesAndNewlines)
        do
        {
            try liststr.write(toFile: Bundle.main.resourcePath! + "applist.txt", atomically: true, encoding: .utf8)
        }
        catch let error
        {
            print("Can't create/write to file:  ", error)
        }
        return listOfApps
    }
    
    func getAppName(input: String) -> [String] // Need to improve this method. It gives output as words which are not found in NB training files. And we assume the words as correct app name due to no time
    {
        let listtoremove = ["a","an","the","of","named","name"]
        
        var r = input.components(separatedBy: .whitespacesAndNewlines)
        let c = try! String(contentsOfFile: Bundle.main.resourcePath! + "/Train/apps.txt")
        let q = c.components(separatedBy: .whitespacesAndNewlines)
        let w = NSCountedSet(array: q)
        var unique = [String]()
        for i in listtoremove
        {
            var c = 0
            for j in r
            {
                if i.lowercased() == j.lowercased()
                {
                    r.remove(at: c)
                }
                c += 1
            }
        }
        //print(r)
        for i in r
        {
            if w.count(for: i) == 0
            {
                unique.append(i)
            }
        }
        return unique
    }
    func open(input: String) -> Bool
    {
        let appName = self.getAppName(input: input)[0]
        var list: [String]
        var opened: Bool = false
        if FileManager.default.fileExists(atPath: Bundle.main.resourcePath! + "applist.txt")
        {
            do
            {
                list = try String(contentsOfFile: Bundle.main.resourcePath! + "applist.txt").components(separatedBy: .newlines)
            }
            catch let error
            {
                print("Can't read applistfile:  ",error)
                list = self.find()
            }
        }
        else
        {
            list = self.find()
        }
        //Should handle same name apps in future
        for i in list
        {
            if i.lowercased().contains(appName.lowercased())
            {
                let ws = NSWorkspace.shared()
                ws.launchApplication(i)
                opened = true
                /*
                let p = Process()
                p.launchPath = "/bin/bash"
                p.arguments = ["-c","open -a \"\(i)\""]
                p.launch()
                 */
            }
        }
        if !opened
        {
            list = self.find()
            for i in list
            {
                if i.lowercased().contains(appName.lowercased())
                {
                    let ws = NSWorkspace.shared()
                    ws.launchApplication(i)
                    opened = true
                    /*
                     let p = Process()
                     p.launchPath = "/bin/bash"
                     p.arguments = ["-c","open -a \"\(i)\""]
                     p.launch()
                     */
                }
            }
            
        }
        return opened
    }
    
    func closeAll()
    {
        let ws = NSWorkspace.shared()
        let apps = ws.runningApplications
        for currentApp in apps
        {
            if(currentApp.activationPolicy == .regular)
            {
                if currentApp.bundleIdentifier != "com.skreweverything.Acolyte"
                {
                    currentApp.forceTerminate()
                    print("close all: ",currentApp.localizedName!)
                }
                
            }
        }
    }
    
    func close(input: String)
    {
        let appName = self.getAppName(input: input)[0]
        let ws = NSWorkspace.shared()
        let apps = ws.runningApplications
        for currentApp in apps
        {
            if(currentApp.activationPolicy == .regular)
            {
                if currentApp.bundleIdentifier == "com.skreweverything.Acolyte"
                {
                    //Don't terminate main app
                }
                if (currentApp.localizedName?.lowercased().contains(appName.lowercased()))!
                {
                    currentApp.forceTerminate()
                }
                
            }
        }
    }
}
