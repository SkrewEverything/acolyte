//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//

#import "sqlite3.h"
#import "Chrome.h"


int IOBluetoothPreferencesAvailable();

int IOBluetoothPreferenceGetControllerPowerState();
void IOBluetoothPreferenceSetControllerPowerState(int state);

int IOBluetoothPreferenceGetDiscoverableState();
void IOBluetoothPreferenceSetDiscoverableState(int state);

