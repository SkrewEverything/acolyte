//
//  SharedNB.swift
//  Brain
//
//  Created by Sri Harish on 08/04/17.
//  Copyright © 2017 Skrew Everything. All rights reserved.
//

import Foundation

class SharedNB
{
    static var pathToTrainingFiles = Bundle.main.resourcePath! + "/Train"
    static var getNBs: [String:NB] = [:]
    static var staticInit: [String]  {
        /*
        let listOfFiles = try! FileManager.default.contentsOfDirectory(atPath: pathToTrainingFiles)
        var classes = [String]()
        var finaldocs = [String]()
        for i in listOfFiles
        {
            /* Removes .txt and returns file name which we use it for class name in NB */
            let start = i.startIndex
            let end = i.index(i.endIndex, offsetBy: -4)
            let range = start..<end
            let className = i.substring(with: range)
            /* */
            var doc = try! String(contentsOfFile: pathToTrainingFiles + "/\(i)")
            doc = doc.trimmingCharacters(in: .newlines)
            let temp = doc.components(separatedBy: .newlines)
            var doc1 = [String]()
            for j in temp
            {
                doc1.append(j.trimmingCharacters(in: .whitespacesAndNewlines))
            }
            for _ in doc1
            {
                
                classes.append(className)
            }
            finaldocs += doc1
        }
        print(finaldocs)
        print(classes)
        */
        var classes = [String]()
        var finaldocs = [String]()
        var doc = ""
        var temp = [String]()
        /***** NB0  *****/
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/capabilities.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("capabilities")
        }
        
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/commands.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("commands")
        }
        let nb0 = NB()
        nb0.train(document: finaldocs, className: classes)
        getNBs["NB0"] = nb0
        //print(finaldocs)
        //print(classes)
        /***************************/
        /****  NB1  *******/
        classes.removeAll()
        finaldocs.removeAll()
        
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/system-capabilities.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("system-capabilities")
        }
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/other-capabilities.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("other-capabilities")
        }
        let nb1 = NB()
        nb1.train(document: finaldocs, className: classes)
        getNBs["NB1"] = nb1
        /***************************/
        /**** NB2  ******/
        classes.removeAll()
        finaldocs.removeAll()
        
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/wifi.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("wifi")
        }
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/bluetooth.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("bluetooth")
        }
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/brightness.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("brightness")
        }
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/volume.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("volume")
        }
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/logs.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("logs")
        }
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/webpage.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("webpage")
        }
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/apps.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("apps")
        }
        
        let nb2 = NB()
        nb2.train(document: finaldocs, className: classes)
        getNBs["NB2"] = nb2
        //print(finaldocs)
        //print(classes)
        /***************************/
        /**** NB3  ******/
        classes.removeAll()
        finaldocs.removeAll()
        
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/mute-volume.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("mute-volume")
        }
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/unmute-volume.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("unmute-volume")
        }
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/increase-volume.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("increase-volume")
        }
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/decrease-volume.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("decrease-volume")
        }
        let nb3 = NB()
        nb3.train(document: finaldocs, className: classes)
        getNBs["NB3"] = nb3
        /***********************/
        /***** NB6 ********/
        classes.removeAll()
        finaldocs.removeAll()
        
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/decrease-brightness.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("decrease-brightness")
        }
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/increase-brightness.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("increase-brightness")
        }
        let nb6 = NB()
        nb6.train(document: finaldocs, className: classes)
        getNBs["NB6"] = nb6
        /***********************/
        /******* NB5 **********/
        classes.removeAll()
        finaldocs.removeAll()
        
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/bluetooth-on.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("bluetooth-on")
        }
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/bluetooth-off.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("bluetooth-off")
        }
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/bluetooth-list.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("bluetooth-list")
        }
        let nb5 = NB()
        nb5.train(document: finaldocs, className: classes)
        getNBs["NB5"] = nb5
        /**********************/
        /********* NB4 *******/
        classes.removeAll()
        finaldocs.removeAll()
        
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/wifi-on.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("wifi-on")
        }
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/wifi-off.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("wifi-off")
        }
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/wifi-list.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("wifi-list")
        }
        let nb4 = NB()
        nb4.train(document: finaldocs, className: classes)
        getNBs["NB4"] = nb4
        /****************************/
        /******* NB8 ********/
        classes.removeAll()
        finaldocs.removeAll()
        
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/open-app.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("open-app")
        }
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/close-app.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("close-app")
        }
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/close-all-apps.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("close-all-apps")
        }
        let nb8 = NB()
        nb8.train(document: finaldocs, className: classes)
        getNBs["NB8"] = nb8
        /*********************/
        /******* NB9 *********/
        classes.removeAll()
        finaldocs.removeAll()
        
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/webpage-back.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("webpage-back")
        }
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/webpage-bookmark.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("webpage-bookmark")
        }
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/webpage-open-window.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("webpage-open-window")
        }
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/webpage-open.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("webpage-open")
        }
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/webpage-reload.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("webpage-reload")
        }
        let nb9 = NB()
        nb9.train(document: finaldocs, className: classes)
        getNBs["NB9"] = nb9
        /********************/
        /******* NB7 *******/
        classes.removeAll()
        finaldocs.removeAll()
        
        doc = try! String(contentsOfFile: pathToTrainingFiles + "/logs.txt")
        doc = doc.trimmingCharacters(in: .newlines)
        temp = doc.components(separatedBy: .newlines)
        for i in temp
        {
            finaldocs.append(i.trimmingCharacters(in: .whitespacesAndNewlines))
            classes.append("logs")
        }
        let nb7 = NB()
        nb7.train(document: finaldocs, className: classes)
        getNBs["NB7"] = nb7
        return []
    }
    init()
    {
        print(SharedNB.staticInit)
    }
}
