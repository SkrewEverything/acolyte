//
//  NerdStatsViewController.swift
//  Acolyte
//
//  Created by Sri Harish on 21/03/17.
//  Copyright © 2017 Skrew Everything. All rights reserved.
//

import Cocoa

class NerdStatsViewController: NSViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        let queue = DispatchQueue(label: "com.skreweverything.nerdstats", qos: .userInteractive, attributes: .concurrent)
        queue.async {
            let process = Process()
            process.launchPath = "/bin/bash"
            process.arguments = ["-c","open -a \"System Information.app\""]
            process.launch()
        }

    }
    
}
