//
//  main.swift
//  FileSystem
//
//  Created by Sri Harish on 23/03/17.
//  Copyright © 2017 Skrew Everything. All rights reserved.
//

import Foundation
import Cocoa


class FileSystem
{
    var path: String = ""
    var find = [String]()
    var lib: String = NSString(string: "~").expandingTildeInPath + "/Library"
    var listOfDirectories: [String] = []
    var listOfFiles: [String] = []
    let fileManager = FileManager.default;
    var c = [String]()
    
    init(path: [String], find: [String]?)
    {
        if let f = find
        {
            self.find = f
        }
        for i in path
        {
            self.path = NSString(string: i).expandingTildeInPath
            start(path: self.path)
            //start()
        }
    }
    func start(path: String)
    {
        do
        {
            let list = try fileManager.contentsOfDirectory(atPath: path)
            for var i in list
            {
                let j = i
                i = path + "/" + i // constructing whole path
                var b: ObjCBool = false
                if fileManager.fileExists(atPath: i, isDirectory: &b) // this is to mainly check file or directory
                {
                    if b.boolValue // if the file is a directory
                    {
                        if j.hasPrefix(".") // hidden directories
                        {
                            continue
                        }
                        else if j.hasSuffix(".app") || j.hasSuffix(".jar") || j.hasSuffix(".xcodeproj") || j.hasSuffix(".framework")  || j.hasSuffix(".photoslibrary") //directories dont go inside
                        {
                            
                            if self.find.count != 0
                            {
                                for f in self.find
                                {
                                    if j.lowercased().contains(f.lowercased())
                                    {
                                        //print(i)
                                        c.append(i)
                                    }
                                }
                            }
                            else
                            {
                                //print(i)
                                c.append(i)
                            }

                            
                        }
                        else if self.lib == i // contains links which goes into infinity.
                        {
                            continue
                        }
                        else
                        {
                            start(path: i) // if its a directory it is called recursively
                        }
                    
                    }
                    else if j.hasPrefix(".") // file is hidden
                    {
                        continue
                    }
                    else
                    {
                        if self.find.count != 0
                        {
                            for f in self.find
                            {
                                if j.lowercased().contains(f.lowercased())
                                {
                                    //print(i)
                                    c.append(i)
                                }
                            }
                        }
                        else
                        {
                            //print(i)
                            c.append(i)
                        }
                    }
                }
            }
        }
        catch _ // most probably araises due to user not having read permissions
        {
            //print("error",error)
            return
        }
    }
 /*
     func start()
    {
        var j = ""
        let d = fileManager.enumerator(atPath: currentPath)
        while let i = d?.nextObject() as? String
        {
            j = currentPath + "/" + i
            print(j)
            
            print("====================================================================================")
        }
    }
 */
}
/*var d = Date().timeIntervalSince1970 * 1000.0
var f = FileSystem(path: ["~"], find: [".key",".ppt"])
print(f.c)
var ff = Date().timeIntervalSince1970 * 1000.0
print(ff-d, " ms")
*/
