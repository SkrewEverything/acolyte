//
//  NB.swift
//  Brain
//
//  Created by Sri Harish on 08/04/17.
//  Copyright © 2017 Skrew Everything. All rights reserved.
//

import Foundation

class NB
{
    var V: Int = 0
    var VwordsSet: Set<String> = []
    var totalWords: Int = 0
    var Nc: Int = 0
    var N: Int = 0
    var PC: Double = 0.0
    var indexes: [String:[Int]] = [:] // same class document indexes as [Int]
    var countC: Int = 0
    var document: [String] = []
    var testdocument: String = ""
    var distinctClasses: Set<String> = []
    var noofdocumentsinaclass = [String:Int]()
    var noofwordsintestdocuments = [String:Int]()
    var pwc: [String:[[String:Double]]] = [:] // [class:[[word:probability]]]
    func train(document: [String], className: [String])
    {
        self.document = document
        var distinctClasses = Set<String>()
        for j in className
        {
            if noofdocumentsinaclass[j] == nil
            {
                noofdocumentsinaclass[j] = 1
            }
            else
            {
                noofdocumentsinaclass[j] = noofdocumentsinaclass[j]! + 1
            }
            distinctClasses.insert(j)
        }
        self.distinctClasses = distinctClasses
        for j in distinctClasses
        {
            var k = 0
            var a: [Int] = []
            for i in className
            {
                if j == i
                {
                    a.append(k)
                }
                k += 1
            }
            indexes[j] = a
            
        }
        for i in document
        {
            let j = i.components(separatedBy: .whitespaces)
            for k in j
            {
                VwordsSet.insert(k)
            }
        }
        V = VwordsSet.count
        N += document.count
        
        for i in distinctClasses
        {
            var set = Set<String>()
            for j in indexes[i]!
            {
                
                let k = document[j].components(separatedBy: .whitespaces)
                
                for o in k
                {
                    set.insert(o)
                }
                
                
            }
            //print("words:  ",set)
            var pwcarray: [[String:Double]] = []
            for m in set
            {
                pwcarray.append([m : calculate(nm: countWC(w: m, c: i), dm: count(c: i))])
            }
            pwc[i] = pwcarray
        }
        
    }
    func calculate(nm: Int, dm: Int) -> Double
    {
        var result: Double = 0.0
        let dnm: Double = Double(nm)
        let ddm: Double = Double(dm)
        result = (dnm + 1.0)/(ddm + Double(V))
        //print("countWC:  ",nm,"  countc:  ",dm," result:  ",result)
        return result
    }
    func countWC(w: String, c: String) -> Int
    {
        var result: Int = 0
        
        let i = indexes[c]
        if let ii = i
        {
            for j in ii
            {
                let k = self.document[j].components(separatedBy: .whitespaces)
                for l in k
                {
                    if w == l
                    {
                        result += 1
                    }
                }
            }
        }
        //print("class: ",c," word: ",w," result: ",result)
        return result
    }
    
    func count(c: String) -> Int
    {
        var result: Int = 0
        
        let i = indexes[c]
        if let ii = i
        {
            for j in ii
            {
                result += self.document[j].components(separatedBy: .whitespaces).count
            }
        }
        //print("result:  ",result)
        return result
    }
    
    func pc(c: String)
    {
        
    }
    func test(document: String) -> [String:Double]
    {
        var prob: [String:[[String:Double]]] = [:]
        var dset = Set<String>()
        
        for i in document.components(separatedBy: .whitespaces)
        {
            if noofwordsintestdocuments[i] == nil
            {
                noofwordsintestdocuments[i] = 1
            }
            else
            {
                noofwordsintestdocuments[i] = noofwordsintestdocuments[i]! + 1
            }
            dset.insert(i)
        }
        
        for i in distinctClasses
        {
            //print("words:  ",set)
            var pwcarray: [[String:Double]] = []
            for m in dset
            {
                pwcarray.append([m : calculate(nm: countWC(w: m, c: i), dm: count(c: i))])
            }
            prob[i] = pwcarray
        }
        //print("noofdocumentsinaclass:  ",noofdocumentsinaclass)
        //print("noofwordsintestdocuments:  ",noofwordsintestdocuments)
        //print("prob:  ",prob)
        var chooseAClass = [String:Double]()
        var tempDIC = [String:Double]()
        for i in prob
        {
            //print(i)
            tempDIC = [String:Double]()
            for k in i.value
            {
                for l in k
                {
                    tempDIC[l.key] = l.value
                }
            }
            //print("TempDIC:   ", tempDIC)
            chooseAClass[i.key] = (Double(noofdocumentsinaclass[i.key]!)/Double(N))
            for j in noofwordsintestdocuments
            {
                chooseAClass[i.key] = chooseAClass[i.key]! * pow(tempDIC[j.key]!, Double(j.value))
            }
            
        }
        //print("Final===========",chooseAClass)
        
        return chooseAClass
    }
}

