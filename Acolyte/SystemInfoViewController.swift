//
//  SystemInfoViewController.swift
//  ProjectPrototype1
//
//  Created by Sri Harish on 16/03/17.
//  Copyright © 2017 Skrew Everything. All rights reserved.
//

import Cocoa

class SystemInfoViewController: NSViewController
{

    @IBOutlet var versionLabel: NSTextField!
    @IBOutlet var productLabel: NSTextField!
    @IBOutlet var processorLabel: NSTextField!
    @IBOutlet var ramLabel: NSTextField!
    @IBOutlet var graphicsLabel: NSTextField!
    @IBOutlet var serialNumberLabel: NSTextField!
    @IBOutlet var availableStorageLabel: NSTextField!
    @IBOutlet var capacityStorageLabel: NSTextField!
    @IBOutlet var computerNameLabel: NSTextField!
    @IBOutlet var userNameLabel: NSTextField!
    @IBOutlet var isLoadingMainWindow: NSProgressIndicator!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do view setup here.
        versionLabel.stringValue = SystemInfoData.systemVersion
        processorLabel.stringValue = SystemInfoData.processorInfo
        ramLabel.stringValue = SystemInfoData.memoryInfo
        graphicsLabel.stringValue = SystemInfoData.graphicsInfo
        serialNumberLabel.stringValue = SystemInfoData.serialNumber
        availableStorageLabel.stringValue = SystemInfoData.availableStorage
        capacityStorageLabel.stringValue = SystemInfoData.capacityStorage
        computerNameLabel.stringValue = SystemInfoData.computerName
        userNameLabel.stringValue = SystemInfoData.userName
        self.isLoadingMainWindow.startAnimation(nil)
        let workItem = DispatchWorkItem
            {
                var data = [String]()
                
                do
                {
                    let sysData = try String(contentsOfFile: Path.toSystemInfoData + "/summary.txt")
                    data = sysData.components(separatedBy: .newlines)
                }
                catch let error
                {
                    print("Can't read data from systeminfo")
                    print(error)
                }
                if data.count > 0
                {
                    self.versionLabel.stringValue = data[0]
                    self.processorLabel.stringValue = data[1]
                    self.ramLabel.stringValue = data[2]
                    self.graphicsLabel.stringValue = data[3]
                    self.serialNumberLabel.stringValue = data[4]
                    let storage = data[5].components(separatedBy: " ")
                    self.availableStorageLabel.stringValue = storage[0]
                    self.capacityStorageLabel.stringValue = storage[1]
                    self.computerNameLabel.stringValue = data[6]
                    self.userNameLabel.stringValue = data[7]
                    /*Update Global Data */
                    SystemInfoData.systemVersion = data[0]
                    SystemInfoData.processorInfo = data[1]
                    SystemInfoData.memoryInfo = data[2]
                    SystemInfoData.graphicsInfo = data[3]
                    SystemInfoData.serialNumber = data[4]
                    SystemInfoData.availableStorage = storage[0]
                    SystemInfoData.capacityStorage = storage[1]
                    SystemInfoData.computerName = data[6]
                    SystemInfoData.userName = data[7]
                }
                self.isLoadingMainWindow.stopAnimation(nil)
        }

        let queue = DispatchQueue(label: "com.skreweverything.systeminfo", qos: .userInteractive, attributes: .concurrent)
        queue.async
            {
                Services.systemInfo = Process()
                Services.systemInfo.launchPath = Path.toSystemInfo
                Services.systemInfo.launch()
                Services.systemInfo.waitUntilExit()
                DispatchQueue.main.async(execute: workItem)
        }
            }
    
}
