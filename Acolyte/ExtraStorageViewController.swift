//
//  ExtraStorageViewController.swift
//  Acolyte
//
//  Created by Sri Harish on 21/03/17.
//  Copyright © 2017 Skrew Everything. All rights reserved.
//

import Cocoa

class ExtraStorageViewController: NSViewController {

    @IBOutlet var textView: NSTextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        /* We can't change font, size and color of textview in Interface Builder */
        textView.font = NSFont(name: "Courier New", size: 13)
        textView.textColor = NSColor.white
        var data: String = ""
        do
        {
            data = try String(contentsOfFile: Path.toSystemInfoData + "/extraStorage.txt")
        }
        catch let error
        {
            print("Can't read data from systeminfo")
            print(error)
        }
        textView.string = data
    }
    
}
