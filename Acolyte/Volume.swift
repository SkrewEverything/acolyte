//
//  Volume.swift
//  Brain
//
//  Created by Sri Harish on 16/04/17.
//  Copyright © 2017 Skrew Everything. All rights reserved.
//

import Foundation

class Volume
{
    func getVolume() -> Double
    {
        let p = Process()
        let pipe = Pipe()
        p.standardOutput = pipe
        p.launchPath = "/bin/bash"
        p.arguments = ["-c","osascript -e \"get volume settings\""]
        p.launch()
        p.waitUntilExit()
        let data = pipe.fileHandleForReading.readDataToEndOfFile()
        let output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
        let r = Regex(regex: "output volume:(\\d+)", string: output)
        let result = Double(r.getCapturedGroups()![0])
        
        return result!
    }
    
    func mute()
    {
        let p = Process()
        p.arguments = ["-c","osascript -e \"set Volume output muted true\""]
        p.launchPath = "/bin/bash"
        p.launch()
    }
    
    func unmute()
    {
        let p = Process()
        p.arguments = ["-c","osascript -e \"set Volume output muted false\""]
        p.launchPath = "/bin/bash"
        p.launch()
    }
    
    func increase()
    {
        let v = getVolume() + 20
        let p = Process()
        p.arguments = ["-c","osascript -e \"set volume output volume \(v)\""]
        p.launchPath = "/bin/bash"
        p.launch()
        p.waitUntilExit()
    }
    
    func decrease()
    {
        let v = getVolume() - 20
        let p = Process()
        p.arguments = ["-c","osascript -e \"set volume output volume \(v)\""]
        p.launchPath = "/bin/bash"
        p.launch()
        p.waitUntilExit()
    }
}
