//
//  ExtraCPUViewController.swift
//  Acolyte
//
//  Created by Sri Harish on 21/03/17.
//  Copyright © 2017 Skrew Everything. All rights reserved.
//

import Cocoa

class ExtraCPUViewController: NSViewController {

    @IBOutlet var textView: NSTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        /* We can't change font, size and color of text view in Interface Builder */
        textView.font = NSFont(name: "Courier New", size: 13)
        textView.textColor = NSColor.white
        var data: String = ""
        do
        {
            data = try String(contentsOfFile: Path.toSystemInfoData + "/extraCPU.txt")
        }
        catch let error
        {
            print("Can't read data from systeminfo")
            print(error)
        }
        data = data.replacingOccurrences(of: "\n", with: "\n\n")
        textView.string = data

    }
    
}
