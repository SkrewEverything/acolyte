//
//  WebDataViewController.swift
//  Acolyte
//
//  Created by Sri Harish on 17/04/17.
//  Copyright © 2017 Skrew Everything. All rights reserved.
//

import Cocoa

class WebDataViewController: NSViewController {

    @IBOutlet var textView: NSTextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        /** For Horizontal scroll */
        self.textView.isHorizontallyResizable = true
        self.textView.textContainer?.widthTracksTextView = false
        self.textView.textContainer?.containerSize = CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude)
        /*************************/
        self.textView.font = NSFont(name: "Courier New", size: 13)
        self.textView.textColor = NSColor.black
        var data = ""
        let path = Bundle.main.resourcePath! + "/Web/chrome.db"
        let db = Database(type: .file(path))
        do
        {
            try db.open()
        }
        catch let error
        {
            print("Can't open db to read in webdataVC:  ",error)
        }
        let stmt = "select * from historyTable;"
        var rstmt: OpaquePointer! = nil
        var resultsFromSELECT: [[Any]]? = nil
        do
        {
            rstmt = try db.prepareStatement(sql: stmt)
            resultsFromSELECT  = db.execute(pstmt: rstmt)
            //print(resultsFromSELECT)
            db.destroyPS(pstmt: rstmt)
        }
        catch let error
        {
            print("In webdataVC: ", error)
        }
        db.close()
        if let results = resultsFromSELECT
        {
            for i in results
            {
                for j in i
                {
                    data = data + "  " + String(describing: j).trimmingCharacters(in: .whitespaces)
                }
                data = data + "\n\n\n"
            }
        }
        self.textView.string = data.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
}
