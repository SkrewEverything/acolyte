//
//  Wifi.swift
//  Brain
//
//  Created by Sri Harish on 15/04/17.
//  Copyright © 2017 Skrew Everything. All rights reserved.
//

import Foundation

class Wifi
{
    func wifiOff()
    {
        let p = Process()
        p.launchPath = "/bin/bash"
        p.arguments = ["-c","networksetup -setairportpower en0 off"]
        p.launch()
    }
    func wifiOn()
    {
        let p = Process()
        p.launchPath = "/bin/bash"
        p.arguments = ["-c","networksetup -setairportpower en0 on"]
        p.launch()
    }
    func wifiList() -> [String]
    {
        let p = Process()
        let pipe = Pipe()
        p.standardOutput = pipe
        p.launchPath = "/bin/bash"
        p.arguments = ["-c","/System/Library/PrivateFrameworks/Apple80211.framework/Versions/A/Resources/airport scan"]
        p.launch()
        p.waitUntilExit()
        let data = pipe.fileHandleForReading.readDataToEndOfFile()
        let result = (NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String).components(separatedBy: .newlines)
        return result
    }
    func wifiConnect(ssid: String, pass: String?) -> String? // returns nil if successful
    {
        let p = Process()
        let pipe = Pipe()
        p.standardOutput = pipe
        p.launchPath = "/bin/bash"
        if let p1 = pass
        {
            p.arguments = ["-c","networksetup -setairportnetwork en0 \(ssid) \(p1)"]
        }
        else
        {
            p.arguments = ["-c","networksetup -setairportnetwork en0 \(ssid)"]
        }
        p.launch()
        p.waitUntilExit()
        let data = pipe.fileHandleForReading.readDataToEndOfFile()
        if String(describing: data) == "0 bytes"
        {
            return nil
        }
        else
        {
            return "Couldn't connect"
        }
        
    }
    func getPassword(ssid: String) -> String?
    {
        let p = Process()
        let pipe = Pipe()
        p.standardError = pipe // password gets in standard error
        p.standardOutput = pipe // to supress extra output
        p.launchPath = "/bin/bash"
        p.arguments = ["-c", "security find-generic-password -D \"AirPort network password\" -a \(ssid) -g"]
        p.launch()
        p.waitUntilExit()
        let data = pipe.fileHandleForReading.readDataToEndOfFile()
        let s = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
        if (s.contains("The specified item could not be found in the keychain."))
        {
            return nil
        }
        else
        {
            let r = Regex(regex: "password: \"(.*)\"", string: s)
            return r.getCapturedGroups()?[0]
        }
    }
}
