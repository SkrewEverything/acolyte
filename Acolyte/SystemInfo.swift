//
//  SystemInfo.swift
//  SystemInfo
//
//  Created by Skrew Everything on 20/03/17.
//  Copyright © 2017 Skrew Everything. All rights reserved.
//

import Foundation

class SystemInfo
{
    var process:Process!
    var pipe:Pipe!
var data: Data!
var output: String = ""
var finalOutput: String = ""
var regex: Regex!
var regexOutput: String = ""
var extraCPU: String = ""
var extraRAM: String = ""
    var extraStorage: String = ""
var path = Bundle.main.resourcePath!
/* Getting Version */
init()
{
    do
    {
        try FileManager.default.createDirectory(atPath: path + "/SystemInfoData", withIntermediateDirectories: true, attributes: nil)
    }
    catch let error
    {
        print("Can't create directory")
        print(error)
    }

process = Process()
pipe = Pipe()
process.launchPath = "/bin/bash"
process.standardOutput = pipe
process.arguments = ["-c","system_profiler SPSoftwareDataType | grep -i \"system version\""]
process.launch()
process.waitUntilExit()
data = pipe.fileHandleForReading.readDataToEndOfFile()
output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
regex = Regex(regex: "System Version.*:\\s(.*)", string: output)
regexOutput = regex.getCapturedGroups()![0]
finalOutput.append(regexOutput + "\n")

/* Getting Product info like year */


/* Getting Processor info */
process = Process()
pipe = Pipe()
process.standardOutput = pipe
process.launchPath = "/bin/bash"
process.arguments = ["-c","system_profiler SPHardwareDataType | grep -i \"Processor Name\""]
process.launch()
process.waitUntilExit()
data = pipe.fileHandleForReading.readDataToEndOfFile()
output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
regex = Regex(regex: "Processor Name.*:\\s(.*)", string: output)
regexOutput = regex.getCapturedGroups()![0]
finalOutput.append(regexOutput + " ")

process = Process()
pipe = Pipe()
process.standardOutput = pipe
process.launchPath = "/bin/bash"
process.arguments = ["-c","system_profiler SPHardwareDataType | grep -i \"Processor Speed\""]
process.launch()
process.waitUntilExit()
data = pipe.fileHandleForReading.readDataToEndOfFile()
output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
regex = Regex(regex: "Processor Speed.*:\\s(.*)", string: output)
regexOutput = regex.getCapturedGroups()![0]
finalOutput.append(regexOutput + "\n")

/* Getting Memory info */
process = Process()
pipe = Pipe()
process.standardOutput = pipe
process.launchPath = "/bin/bash"
process.arguments = ["-c","system_profiler SPHardwareDataType | grep -i memory"]
process.launch()
process.waitUntilExit()
data = pipe.fileHandleForReading.readDataToEndOfFile()
output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
regex = Regex(regex: "Memory.*:\\s(.*)", string: output)
regexOutput = regex.getCapturedGroups()![0]
finalOutput.append(regexOutput + " ")

process = Process()
pipe = Pipe()
process.standardOutput = pipe
process.launchPath = "/bin/bash"
process.arguments = ["-c","system_profiler SPMemoryDataType | grep -i speed"]
process.launch()
process.waitUntilExit()
data = pipe.fileHandleForReading.readDataToEndOfFile()
output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
regex = Regex(regex: "Speed.*:\\s(.*)", string: output)
regexOutput = regex.getCapturedGroups()![0]
finalOutput.append(regexOutput + " ")

process = Process()
pipe = Pipe()
process.standardOutput = pipe
process.launchPath = "/bin/bash"
process.arguments = ["-c","system_profiler SPMemoryDataType | grep -i type"]
process.launch()
process.waitUntilExit()
data = pipe.fileHandleForReading.readDataToEndOfFile()
output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
regex = Regex(regex: "Type.*:\\s(.*)", string: output)
regexOutput = regex.getCapturedGroups()![0]
finalOutput.append(regexOutput + "\n")

/* Getting Graphics info */
process = Process()
pipe = Pipe()
process.standardOutput = pipe
process.launchPath = "/bin/bash"
process.arguments = ["-c","system_profiler SPDisplaysDataType | grep -i \"chipset model\""]
process.launch()
process.waitUntilExit()
data = pipe.fileHandleForReading.readDataToEndOfFile()
output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
regex = Regex(regex: "Chipset Model.*:\\s(.*)", string: output)
regexOutput = regex.getCapturedGroups()![0]
finalOutput.append(regexOutput + " ")

process = Process()
pipe = Pipe()
process.standardOutput = pipe
process.launchPath = "/bin/bash"
process.arguments = ["-c","system_profiler SPDisplaysDataType | grep -i vram"]
process.launch()
process.waitUntilExit()
data = pipe.fileHandleForReading.readDataToEndOfFile()
output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
regex = Regex(regex: "VRAM.*:\\s(.*)", string: output)
regexOutput = regex.getCapturedGroups()![0]
finalOutput.append(regexOutput + "\n")

/* Getting Serial Number */
process = Process()
pipe = Pipe()
process.standardOutput = pipe
process.launchPath = "/bin/bash"
process.arguments = ["-c","system_profiler SPHardwareDataType | grep -i \"serial number\""]
process.launch()
process.waitUntilExit()
data = pipe.fileHandleForReading.readDataToEndOfFile()
output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
regex = Regex(regex: "Serial Number.*:\\s(.*)", string: output)
regexOutput = regex.getCapturedGroups()![0]
finalOutput.append(regexOutput + "\n")

    /* Getting storage */
    process = Process()
    pipe = Pipe()
    process.standardOutput = pipe
    process.launchPath = "/bin/bash"
    process.arguments = ["-c","system_profiler SPStorageDataType | grep -i available"]
    process.launch()
    process.waitUntilExit()
    data = pipe.fileHandleForReading.readDataToEndOfFile()
    output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
    regex = Regex(regex: "Available.*:\\s(.*)\\sGB", string: output)
    regexOutput = regex.getCapturedGroups()![0]
    finalOutput.append(regexOutput + " ")
    
    process = Process()
    pipe = Pipe()
    process.standardOutput = pipe
    process.launchPath = "/bin/bash"
    process.arguments = ["-c","system_profiler SPStorageDataType | grep -i capacity"]
    process.launch()
    process.waitUntilExit()
    data = pipe.fileHandleForReading.readDataToEndOfFile()
    output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
    regex = Regex(regex: "Capacity.*:\\s(.*)\\sGB", string: output)
    regexOutput = regex.getCapturedGroups()![0]
    finalOutput.append(regexOutput + "\n")
    

    
/* Getting Computer Name */
process = Process()
pipe = Pipe()
process.standardOutput = pipe
process.launchPath = "/bin/bash"
process.arguments = ["-c","system_profiler SPSoftwareDataType | grep -i \"computer name\""]
process.launch()
process.waitUntilExit()
data = pipe.fileHandleForReading.readDataToEndOfFile()
output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
regex = Regex(regex: "Computer Name.*:\\s(.*)", string: output)
regexOutput = regex.getCapturedGroups()![0]
finalOutput.append(regexOutput + "\n")

/* Getting User Name */
process = Process()
pipe = Pipe()
process.standardOutput = pipe
process.launchPath = "/bin/bash"
process.arguments = ["-c","system_profiler SPSoftwareDataType | grep -i \"user name\""]
process.launch()
process.waitUntilExit()
data = pipe.fileHandleForReading.readDataToEndOfFile()
output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
regex = Regex(regex: "User Name.*:\\s(.*)", string: output)
regexOutput = regex.getCapturedGroups()![0]
finalOutput.append(regexOutput + "\n")

//print(finalOutput)
do
{
    try finalOutput.write(toFile: path+"/summary.txt", atomically: false, encoding: .utf8)
}
catch let error
{
    print("Can't create file")
    print(error)
}

/* Extra info about CPU */
process = Process()
pipe = Pipe()
process.standardOutput = pipe
process.launchPath = "/bin/bash"
process.arguments = ["-c","system_profiler SPHardwareDataType | grep -i \"Number of Processors\""]
process.launch()
process.waitUntilExit()
data = pipe.fileHandleForReading.readDataToEndOfFile()
output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
extraCPU.append(output.trimmingCharacters(in: .whitespaces))

process = Process()
pipe = Pipe()
process.standardOutput = pipe
process.launchPath = "/bin/bash"
process.arguments = ["-c","system_profiler SPHardwareDataType | grep -i \"Total Number of Cores\""]
process.launch()
process.waitUntilExit()
data = pipe.fileHandleForReading.readDataToEndOfFile()
output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
extraCPU.append(output.trimmingCharacters(in: .whitespaces))


process = Process()
pipe = Pipe()
process.standardOutput = pipe
process.launchPath = "/bin/bash"
process.arguments = ["-c","system_profiler SPHardwareDataType | grep -i \"L2 Cache (per Core)\""]
process.launch()
process.waitUntilExit()
data = pipe.fileHandleForReading.readDataToEndOfFile()
output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
extraCPU.append(output.trimmingCharacters(in: .whitespaces))

process = Process()
pipe = Pipe()
process.standardOutput = pipe
process.launchPath = "/bin/bash"
process.arguments = ["-c","system_profiler SPHardwareDataType | grep -i \"L3 Cache\""]
process.launch()
process.waitUntilExit()
data = pipe.fileHandleForReading.readDataToEndOfFile()
output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
extraCPU.append(output.trimmingCharacters(in: .whitespaces))

//print(extraCPU)
do
{
    try extraCPU.write(toFile: path + "/extraCPU.txt", atomically: false, encoding: .utf8)
}
catch let error
{
    print("Can't create file")
    print(error)
}

/* Extra RAM info */

process = Process()
pipe = Pipe()
process.standardOutput = pipe
process.launchPath = "/bin/bash"
process.arguments = ["-c","system_profiler SPMemoryDataType"]
process.launch()
process.waitUntilExit()
data = pipe.fileHandleForReading.readDataToEndOfFile()
output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
extraRAM.append(output.trimmingCharacters(in: .whitespaces))

//print(extraRAM)
do
{
    try extraRAM.write(toFile: path + "/extraRAM.txt", atomically: false, encoding: .utf8)
}
catch let error
{
    print("Can't create file")
    print(error)
}
    /*Extra Storage Info */
    
    process = Process()
    pipe = Pipe()
    process.standardOutput = pipe
    process.launchPath = "/bin/bash"
    process.arguments = ["-c","system_profiler SPStorageDataType"]
    process.launch()
    process.waitUntilExit()
    data = pipe.fileHandleForReading.readDataToEndOfFile()
    output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
    extraStorage.append(output.trimmingCharacters(in: .whitespaces))
    
    //print(extraStorage)
    do
    {
        try extraStorage.write(toFile: path + "/SystemInfoData/extraStorage.txt", atomically: true, encoding: .utf8)
    }
    catch let error
    {
        print("Can't create file")
        print(error)
    }

}
    
}
