//
//  main.swift
//  Database
//
//  Created by Sri Harish on 06/03/17.
//  Copyright © 2017 Skrew Everything. All rights reserved.
//
/* https://www.sqlite.org/cintro.html */
/* http://zetcode.com/db/sqlitec/ */

import Foundation



public class Database
{
    public enum SQLError: Error
    {
        case SQLErrorMessage(msg: String)
    }
    public enum DBType
    {
        case inMemory
        case file(String)
    }
    enum dataType: Int32
    {
        case SQLITE_INTEGER = 1, SQLITE_FLOAT, SQLITE3_TEXT, SQLITE_BLOB, SQLITE_NULL
    }
    var db: OpaquePointer? = nil
    var databaseName: String = ""
    /**
     The database connection object
     - parameter type: There are 2 possible values.
        - DBType.inMemory: Creates Database in RAM
        - DBType.file(String): Creates or opens Database file specified by name as an argument
     - returns: Database object which can be used to open, execute, close.
    */
    init(type: DBType)
    {
        switch type
        {
        case .inMemory:
            self.databaseName = ":memory:"
        case .file(let databaseName):
            self.databaseName = databaseName
        }
    }
    /**
     Open a connection to a new or existing SQLite database.
     */
    func open() throws
    {
        let rc = sqlite3_open(databaseName, &self.db)
        if rc != SQLITE_OK
        {
            let msg = "Can't open database. Something went wrong!" + " Error: " + String(cString:sqlite3_errmsg(self.db))
            throw SQLError.SQLErrorMessage(msg: msg)
        }
        else
        {
            //print("Sccessfully created database!")
        }
    }
    /**
    Compile SQL text into byte-code that will do the work of querying or updating the database.
    - parameter sql: SQL command as String
    - returns: Prepared Statement for querying or updating the database.
    */
    func prepareStatement(sql: String) throws -> OpaquePointer
    {
        var statement: OpaquePointer? = nil
        let rc = sqlite3_prepare_v2(db, sql, -1, &statement, nil)
        if rc != SQLITE_OK
        {
            let msg = "Can't prepare statements!" + " Error: " + String(cString: sqlite3_errmsg(self.db))
            throw SQLError.SQLErrorMessage(msg: msg)
        }
        else
        {
            //print("Successfully prepared!")
        }
        if statement == nil
        {
            let msg = "Something went wrong! " + " The prepare statement returned is null " + " Error: " + String(cString: sqlite3_errmsg(self.db))
            throw SQLError.SQLErrorMessage(msg: msg)
        }
        return statement!
    }
    
    /**
    Used to display results when using SELECT command
    - parameter pstmt: It is returned from prepareStatement(sql:)
    - returns: Multi-dimensional array of type AnyObject. Rows and columns of array represents rows and columns of table
 
    */
    func execute(pstmt: OpaquePointer) -> [[Any]]
    {
        var data = [[Any]]()
        var data1 = [Any]()
        while true
        {
            if sqlite3_step(pstmt) == SQLITE_ROW
            {
                for i in 0..<sqlite3_column_count(pstmt)
                {
                    let type = sqlite3_column_type(pstmt, i)
                    switch type
                    {
                    case dataType.SQLITE_INTEGER.rawValue:
                        data1.append(sqlite3_column_int(pstmt, i))
                    case dataType.SQLITE_FLOAT.rawValue:
                        data1.append(sqlite3_column_double(pstmt, i))
                    case dataType.SQLITE3_TEXT.rawValue:
                        data1.append(String(cString: sqlite3_column_text(pstmt, i)))
                    default:
                        print("It might be NULL or BLOB!")
                    }
                }
                data.append(data1)
                data1.removeAll()
            }
            else
            {
                break;
            }
        }
        sqlite3_reset(pstmt)
        return data
    }
    /**
    Executes SQL statements to insert, update, delete. If using SELECT, use execute(pstmt:) -> [[AnyObject]]
     - parameter pstmt: Prepared Statement returned from prepareStatement(sql:)
     - returns: Number of rows effected. 
                -1 if any error occurred
    */
    func execute(pstmt: OpaquePointer) throws -> Int
    {
        if sqlite3_step(pstmt) == SQLITE_DONE
        {
            //print("Successfully Executed")
            sqlite3_reset(pstmt)
            return Int(sqlite3_changes(db))
        }
        else
        {
            let msg = "Didn't execute! Think of using another execute(pstmt:)" + " Error: " + String(cString: sqlite3_errmsg(self.db))
            throw SQLError.SQLErrorMessage(msg: msg)
        }
    }
    /**
     Closes prepared statement in order to avoid resource leaks.
     - parameter pstmt: Prepared Statement returned from prepareStatement(sql:)
    */
    func destroyPS(pstmt: OpaquePointer)
    {
        sqlite3_finalize(pstmt)
    }
    /**
    Closes a connection to Database
     */
    func close()
    {
        sqlite3_close(self.db)
    }
}
