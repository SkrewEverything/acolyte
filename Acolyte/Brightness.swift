//
//  Brightness.swift
//  Brain
//
//  Created by Sri Harish on 13/04/17.
//  Copyright © 2017 Skrew Everything. All rights reserved.
//

import Foundation

class Brightness
{
    func setBrightness(level: Float)// Takes value from 0 to 1.0
    {
        let service = IOServiceGetMatchingService(kIOMasterPortDefault, IOServiceMatching("IODisplayConnect"))
        IODisplaySetFloatParameter(service, 0, kIODisplayBrightnessKey as CFString!, level)
        IOObjectRelease(service)
    }
    
    func getBrightness() -> Float
    {
        let service = IOServiceGetMatchingService(kIOMasterPortDefault, IOServiceMatching("IODisplayConnect"))
        var result: Float = 0.0
        IODisplayGetFloatParameter(service, 0, kIODisplayBrightnessKey as CFString!, &result)
        print("Current Level:  ", result)
        IOObjectRelease(service)
        return result
    }
}
