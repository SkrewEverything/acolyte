//
//  LogsViewController.swift
//  ProjectPrototype1
//
//  Created by Sri Harish on 16/03/17.
//  Copyright © 2017 Skrew Everything. All rights reserved.
//

import Cocoa

class LogsViewController: NSViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        
        
    }
    @IBAction func keyloggerData(_ sender: Any)
    {
        let p = Process()
        let path = Bundle.main.resourcePath! + "/Data/Key"
        p.launchPath = "/bin/bash"
        p.arguments = ["-c","open \"\(path)\""]
        p.launch()
    }
    @IBAction func appLoggingData(_ sender: Any)
    {
        let p = Process()
        let path = Bundle.main.resourcePath! + "/Data/App"
        p.launchPath = "/bin/bash"
        p.arguments = ["-c","open \"\(path)\""]
        p.launch()
    }
    @IBAction func inputDevicesData(_ sender: Any)
    {
        let p = Process()
        let path = Bundle.main.resourcePath! + "/Data/Devices"
        p.launchPath = "/bin/bash"
        p.arguments = ["-c","open \"\(path)\""]
        p.launch()
    }
    
}
