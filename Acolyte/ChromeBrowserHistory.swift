//
//  main.swift
//  applescript
//
//  Created by Skrew Everything on 05/03/17.
//  Copyright © 2017 Skrew Everything. All rights reserved.
//

import Cocoa
import ScriptingBridge

/**
Chrome web browser which tracks opened sites for saving history
If more than 2 sites with same url are opened, it only considers 1 url and only stores 1 entry.
 
- Takes time in seconds which specifies how much time it should wait to scan web browser each time.
*/
public class ChromeBrowserHistory
{
    
    private var timer = Timer()
    private var webpageTitle: String = ""   // Webpage Title
    private var webpageURL: String = ""     // Webpage URL
    private var date: String = ""           // Date when scan performed
    private var time: String = ""           // Time when scan performed
    private var mode: Int = 0          // 0 for normal window, 1 for incognito window
    private var previousList: [[Any]] = []  // Previous list to check if the current webpages list have duplicates or not
    private var currentList: [[Any]] = []   // Current list of tabs in all windows
    private var outputList: [[Any]] = []    // Only distinct values after removing duplicates values from previousList and currentList
    private var chromeObject: AnyObject = SBApplication.init(bundleIdentifier: "com.google.Chrome")!
    private var path: String = Bundle.main.resourcePath! + "/Web" // Creates Web folder in resources folder and db is stored in Web folder
    private var pathToDB: String
    public var db: Database! = nil         // Database points to pathToDB
    private var historyTableInsert: String = "insert into historyTable values("
    private var dbOpened: Bool = false
    init()
    {
        self.pathToDB = path + "/chrome.db"
    }
    /**
    Starts chrome web browser which tracks opened sites for saving history
    If more than 2 sites with same url are opened, it only considers 1 url and only stores 1 entry.
    
    - parameter time: Takes time in seconds which specifies how much time it should wait to scan web browser each time.
 
     */
    public func startTimer(time: Double)
    {
        timer = Timer.scheduledTimer(timeInterval: time, target: self, selector: #selector(run), userInfo: nil, repeats: true)
    }
    /**
    Stops the logging
    */
    public func stop()
    {
        timer.invalidate()
        if db != nil
        {
            db.close()
        }
        dbOpened = false
    }
    @objc private func run()
    {
        let chromeWindowsList = self.chromeObject.windows()
        let calendar = Calendar.current
        self.currentList = []
        self.outputList = []
        for eachWindow in chromeWindowsList!
        {
            let chromeTabsListInEachWindow = (eachWindow as AnyObject).tabs()
            for j in chromeTabsListInEachWindow!
            {
                self.webpageTitle = (j as AnyObject).title
                self.webpageURL = (j as AnyObject).url
                let mode: String = (eachWindow as AnyObject).mode
                if mode.caseInsensitiveCompare("incognito") == .orderedSame
                {
                    //print("incognito")
                    self.mode = 1
                }
                else
                {
                    //print("normal")
                    self.mode = 0
                }
                self.date = "\(calendar.component(.day, from: Date()))-0\(calendar.component(.month, from: Date()))-\(calendar.component(.year, from: Date()))"
                self.time = "\(calendar.component(.hour, from: Date())):\(calendar.component(.minute, from: Date()))"
                let eachEntry: [Any] = [self.webpageTitle,self.webpageURL,self.date,self.time,self.mode]
                currentList.append(eachEntry)
            }
        }
        if self.previousList.count == 0
        {
            self.previousList = self.currentList
            self.outputList = self.currentList
        }
        else
        {
            var flag = true
            for i in self.currentList
            {
                for j in self.previousList
                {
                    if i[1] as! String == j[1] as! String
                    {
                        flag = false
                        break
                    }
                    else
                    {
                        flag = true
                    }
                }
                if flag
                {
                    outputList.append(i)
                }
            }
        }
        previousList = currentList
        //print("OUTPUT LIST:  ",outputList)
        if outputList.count != 0
        {
            storeInDB()
        }
    }
    private func storeInDB()
    {
        
        var pstmt: OpaquePointer! = nil
        var Insertstmt: String = ""
        do
        {
            if !self.dbOpened
            {
                if FileManager.default.fileExists(atPath: pathToDB)
                {
                    self.db = Database(type: .file(pathToDB))
                    try self.db.open()
                    self.dbOpened = true
                }
            
                else
                {
                    try FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: false, attributes: nil)
                    self.db = Database(type: .file(pathToDB))
                    try self.db.open()
                    self.dbOpened = true
                    let createTable = "CREATE TABLE historyTable(date TEXT, webpagetitle TEXT, webpageAddress TEXT, time TEXT, mode INTEGER);"
                    pstmt = try self.db.prepareStatement(sql: createTable)
                    let _:Int = try self.db.execute(pstmt: pstmt)
                    self.db.destroyPS(pstmt: pstmt)
                }
            }
            for var i in outputList
            {
                let temp1 = historyTableInsert  + "\"\(i[2])\""
                let temp2 = ",\"\(i[0])\""
                let temp3 = ",\"\(i[1])\",\"\(i[3])\",\(i[4]));"
                Insertstmt = temp1 + temp2 + temp3
                pstmt = try self.db.prepareStatement(sql: Insertstmt)
                let _:Int = try self.db.execute(pstmt: pstmt)
                self.db.destroyPS(pstmt: pstmt)
                
            }
        }
        catch let msg
        {
            print(msg)
        }
    }
}
