//
//  WifiPass.swift
//  Acolyte
//
//  Created by Sri Harish on 16/04/17.
//  Copyright © 2017 Skrew Everything. All rights reserved.
//

import Cocoa

class WifiPass: NSViewController {

    
    @IBOutlet var wifiPassLabel: NSTextField!
    @IBOutlet var passField: NSSecureTextField!
    let queue = DispatchQueue(label: "com.skreweverything.wifipass", qos: .userInteractive, attributes: .concurrent)
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do view setup here.
        self.wifiPassLabel.stringValue = "The Wi-Fi network \"\(CurrentWifi.ssid)\" requires a password."
    }
    
    @IBAction func wifipassField(_ sender: NSSecureTextField)
    {
        queue.async
        {
            let w = Wifi()
            let r = w.wifiConnect(ssid: CurrentWifi.ssid, pass: sender.stringValue)
            DispatchQueue.main.async {
                
            if r != nil
            {
                let storyBoard = NSStoryboard(name: "Main", bundle: nil) as NSStoryboard
                let myViewController = storyBoard.instantiateController(withIdentifier: "wifi-pass-wrong") as! NSViewController
                self.presentViewControllerAsSheet(myViewController)
            }
            }
        }
    }
    @IBAction func wifiJoin(_ sender: Any)
    {
        self.queue.async
        {
            let w = Wifi()
            let r = w.wifiConnect(ssid: CurrentWifi.ssid, pass: self.passField.stringValue)
            DispatchQueue.main.async {
                
            if r != nil
            {
                let storyBoard = NSStoryboard(name: "Main", bundle: nil) as NSStoryboard
                let myViewController = storyBoard.instantiateController(withIdentifier: "wifi-pass-wrong") as! NSViewController
                self.presentViewControllerAsSheet(myViewController)
            }
            }
        }
    }
}
