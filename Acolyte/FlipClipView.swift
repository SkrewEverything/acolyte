//
//  FlipClipView.swift
//  Acolyte
//
//  Created by Sri Harish on 02/04/17.
//  Copyright © 2017 Skrew Everything. All rights reserved.
//

import Cocoa

class FlipClipView: NSClipView {

    override var isFlipped: Bool { return true }
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)

        // Drawing code here.
    }
    
}
