//
//  ViewController.swift
//  ProjectPrototype1
//
//  Created by Sri Harish on 15/03/17.
//  Copyright © 2017 Skrew Everything. All rights reserved.
//

import Cocoa
var msg = ""
class ViewController: NSViewController
{

    @IBOutlet weak var acolyteLabel: NSTextField!
    @IBOutlet weak var scrollView: NSScrollView!
    @IBOutlet weak var eyesImage: NSImageView!
    var labelCount: Int = 0
    var stack = NSStackView()
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        acolyteLabel.wantsLayer = true
        acolyteLabel.backgroundColor = NSColor.clear
        eyesImage.animates = true
        let eyeImagepath: String = Bundle.main.resourcePath! + "/eyes.gif"
        let url = NSURL(fileURLWithPath: eyeImagepath)
        eyesImage.image = NSImage(byReferencing: url as URL)
        scrollView.scrollsDynamically = true
        scrollView.documentView = stack
        stack.orientation = .vertical
        stack.spacing = 5
        stack.alignment = .left
        stack.edgeInsets = .init(top: 0, left: 0, bottom: 0, right: 0)
        
        // Do any additional setup after loading the view.
    }

    override var representedObject: Any?
    {
        didSet
        {
        // Update the view, if already loaded.
        }
    }
    /**
    Opens Start/Stop Window
    */
    @IBAction func openStartStopWindow(_ sender: Any)
    {
        let sb = NSStoryboard(name: "StartStop", bundle: nil)
        
        Windows.startStopWindow = sb.instantiateInitialController() as! NSWindowController?
        if Windows.startStopWindow.window?.isVisible == false
        {
            Windows.startStopWindow.window?.setIsVisible(true)
        }
        Windows.startStopWindow.window?.isOpaque = false
        Windows.startStopWindow.window?.backgroundColor = NSColor.clear
        Windows.startStopWindow.window?.isMovableByWindowBackground = true

    }
    /**
     Opens System Info Window
     */
    @IBAction func openSystemInfoWindow(_ sender: Any)
    {
        let sb = NSStoryboard(name: "SystemInfo", bundle: nil)
        
        Windows.systemInfoWindow = sb.instantiateInitialController() as! NSWindowController?
        if Windows.systemInfoWindow?.window?.isVisible == false
        {
            Windows.systemInfoWindow?.window?.setIsVisible(true)
        }
        
    }
    /**
     Opens Logs Window
     */
    @IBAction func openLogsWindow(_ sender: Any)
    {
        let sb = NSStoryboard(name: "Logs", bundle: nil)
        
        Windows.logWindow = sb.instantiateInitialController() as! NSWindowController?
        if Windows.logWindow?.window?.isVisible == false
        {
            Windows.logWindow?.window?.setIsVisible(true)
        }
    }
    /**
     Opens Info Window
     */
    @IBAction func openInfoWindow(_ sender: Any)
    {
        let sb = NSStoryboard(name: "Info", bundle: nil)
        
        Windows.infoWindow = sb.instantiateInitialController() as! NSWindowController?
        if Windows.infoWindow?.window?.isVisible == false
        {
            Windows.infoWindow?.window?.setIsVisible(true)
        }

    }
    /**
     When the user entered command and pressed enter
    */
    @IBAction func finishedTyping(_ sender: NSTextField)
    {
        print(sender.stringValue)
        let offset = labelCount*20
        let label = NSTextField(frame: NSMakeRect(0, CGFloat(offset), 100, 20))
        label.isBordered = false
        label.isEditable = false
        label.preferredMaxLayoutWidth = 235
        label.stringValue = sender.stringValue
        //scrollView.documentView?.setFrameSize(CGSize(width: 469, height: CGFloat(labelCount) * 20 + 20))
        stack.addView(label, in: .top)
        let size = stack.fittingSize
        stack.setFrameSize(CGSize(width: 469, height: size.height))
        
        //scrollView.documentView?.addSubview(label)
        //scrollView.contentView.scroll(NSPoint(x: 0, y: offset))
        scrollView.contentView.scroll(NSPoint(x: 0, y: size.height))
        
        labelCount += 1
        self.userInput(input: sender.stringValue)
    }
    
    
    func userInput(input: String)
    {
        let _ = SharedNB()
        BrainStruct.brain = Brain(input: input)
        let presentwifiVC = BrainStruct.brain.wifivc
        if presentwifiVC
        {
            let storyBoard = NSStoryboard(name: "Main", bundle: nil) as NSStoryboard
            let myViewController = storyBoard.instantiateController(withIdentifier: "wifi") as! NSViewController
            self.presentViewControllerAsSheet(myViewController)
        }
    }
}

